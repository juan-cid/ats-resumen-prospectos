﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PruebaAzureStorage.Models
{
    public class ProspectosRepository
    {
        private string connectionString;

        public ProspectosRepository()
        {
            connectionString = @"Data Source = 190.107.176.12,1433; Initial Catalog = aumentat_; User ID = DesaTI; Password = AdminTI2020;";
        }


        public IDbConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }

        public bool put(Prospectos prospecto)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string sQuery = "insert into  [aumentat_].[dbo].[ATS_Prospectos_Testv1] (nombre,rut,email,sueldo,afp_origen,cod_Afp_origen,fono_fijo,fono_movil,fec_ing_reg,canal,horario) values ";
                sQuery += "('" + prospecto.Nombre + "','" + prospecto.Rut + "','" + prospecto.Email + "','" + prospecto.Sueldo + "','" + prospecto.Afp_Origen + "','" + prospecto.Cod_Afp_Origen + "','" + prospecto.Fono_Fijo + "','" + prospecto.Fono_Movil + "','" + prospecto.Fec_ing_reg + "','" + prospecto.Canal + "','" + prospecto.Horario + "')";

                DynamicParameters parameters = new DynamicParameters();
                connection.Open();
                int resultado = 0;
                try
                {
                    resultado = SqlMapper.Execute(connection, sQuery);
                    if (resultado > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    return false;
                }
                



            }
            return false;
        }


        public List<Prospectos> GetAllxFecha(string fecha)
        {
            using (var connection = new SqlConnection(connectionString))
            {
                string sQuery = "select * from  [aumentat_].[dbo].[ATS_Prospectos_Testv1] where fec_ing_reg like '%" + fecha + "%' and fec_ing_reg is not null";

                List<Prospectos> ListaProspectos = new List<Prospectos>();
                ListaProspectos =  connection.Query<Prospectos>(sQuery).ToList();
                return ListaProspectos;
            }
               
            


        }



    }
}
